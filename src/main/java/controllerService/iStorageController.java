package controllerService;

import java.util.ArrayList;


public interface iStorageController {

	public boolean loginCheck( String username, String password, String storageType );

	public ArrayList<String> getPersonalInfo( String username, String password, String storageType );

	public String getPersonalInfoToString( String username, String password, String storageType );

	public ArrayList<String> getGrades( String username, String password, String storageType );

	public String getGradesToString( String username, String password, String storageType );
}